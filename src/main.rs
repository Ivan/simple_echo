use std::{io::BufRead, collections::HashMap, borrow::Cow,
    process::{exit, Command, Stdio},
};

use fdt::Fdt;

fn test_clone(input: &str) {
    let aaa = small::String::from(input);
    let bbb = aaa.clone();

    println!("aaa = {:?}, b = {:?}", aaa, bbb);
}

fn test_fork() {
    let mut cnt = 0;
    while cnt < 128 {
        let mut cmd = Command::new("PROMPT");

        let res = cmd.spawn();
        println!("cnt = {}, spawn = {:?}", cnt, res);
        cnt += 1;
    }
}

fn test_hash_map(map: &mut HashMap<small::String, small::String>, key: &str, val: &str) {
    println!("try to insert hashmap k = {}, v = {}", key, val);
    map.insert(key.into(), val.into());
}

fn test_string(input: &str, depth: usize, expected: usize) {

    println!("before in depth {}", depth);
    let expanded = small::String::from(input);
    println!("after from &str {:?}", expanded);
    if depth == expected {
        return ;
    } else {
        test_string(input, depth + 1, expected);
        println!("after in depth {}, expanded = {:?}", depth, expanded);
    }
}

fn expand(
    output: &mut small::String,
    text: &str,
    do_glob: bool,
    tilde: bool,
) -> Result<(), ()> {
    println!("expand {}", text);
    let concat: small::String = match output.rfind(char::is_whitespace) {
        Some(sep) => {
            if sep == output.len() - 1 {
                text.into()
            } else {
                let word_start = sep + 1;
                let mut t: small::String = output.split_at(word_start).1.into();
                t.push_str(text);
                output.truncate(word_start);
                t
            }
        }
        None => {
            if output.is_empty() {

                println!("output is empty, try into {}", text);
                text.into()
            } else {
                let mut t = output.clone();
                t.push_str(text);
                output.clear();
                t
            }
        }
    };

    let expanded: small::String = concat ;

    println!("push_str 1111 expand:{:p}", std::ptr::addr_of!(expanded));
    output.push_str(prepare_path_for_os(&expanded).as_ref());
    println!("push_str 2222 expand:{:p} output = {}", std::ptr::addr_of!(expanded), output);
    Ok(())
}

fn prepare_path_for_os<'a>(to_trim_away: &'a str) -> Cow<'a, str> {
    if cfg!(target_os = "redox") {
        trim_url_schemes_for_redox_path(to_trim_away)
    } else {
        to_trim_away.into()
    }
}

fn trim_url_schemes_for_redox_path<'a>(to_trim_away: &'a str) -> Cow<'a, str> {
    if let Some(trimmed) = to_trim_away.strip_prefix("file:") {
        if !trimmed.starts_with("/") {
            let mut prefixed_with_slash = String::with_capacity(trimmed.len() + 1);
            prefixed_with_slash.push('/');
            prefixed_with_slash.push_str(trimmed);

            Cow::Owned(prefixed_with_slash)
        } else {
            trimmed.into()
        }
    } else {
        to_trim_away.into()
    }
}

fn test_expand(input: &str) {
	let mut output: small::String = small::String::new();
	expand(&mut output, input, false, false).unwrap();
}

fn main_bak() {
    println!("Hello, world!");
    let mut lines = std::io::stdin().lock().lines();
    let mut map = HashMap::new();
    while let Some(line) = lines.next() {
        let input = line.unwrap();
        let input = input.trim();
        println!("your input {} ", input);

        if input == "exit" {
            println!("GoodBye!");
            break;
        } else if input == "fork" {
            test_fork();
        } else if input.contains("expand") {
            let mut iter = input.split_whitespace();
            iter.next().unwrap();
            let a = iter.next().unwrap();
            test_expand(a);
        } else if input.contains("hash") {
            let mut iter = input.split_whitespace();
            iter.next().unwrap();
            let a = iter.next().unwrap();
            let b = iter.next().unwrap();
            test_hash_map(&mut map, a, b);
        } else if input == "clone" {
            test_clone(input);
        } else {
            let mut iter = input.split_whitespace();
            let a = iter.next().unwrap();
            let b = iter.next().unwrap();
            println!("execute test_string {} with {}", a, b);
            test_string(a, 0, b.parse().unwrap());
        }


    }
}

fn main() {
    let dtb_data = std::fs::read("kernel/dtb:").unwrap();
    println!("read from kernel/dtb, len = {}", dtb_data.len());
    if dtb_data.len() != 0 {
        if let Ok(fdt) = Fdt::new(&dtb_data) {
            println!("DTB model = {}", fdt.root().model());
        } else {
            println!("failed to parse dtb data");
        }
    }
}











